﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace DBScriptGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            String sqlServerLogin = "dika";
            String password = "1234";
            String instanceName = "MSSQLSERVER2014";
            String remoteSvrName = "MTNB117";
            string dbName = "eSICPSD_DEV";

            // Connecting to a named instance of SQL Server with SQL Server Authentication using ServerConnection
            ServerConnection srvConn = new ServerConnection();
            srvConn.ServerInstance = remoteSvrName+@"\" + instanceName;   // connects to named instance
            srvConn.LoginSecure = false;   // set to true for Windows Authentication
            srvConn.Login = sqlServerLogin;
            srvConn.Password = password;
            Server srv = new Server(srvConn);
            Console.WriteLine(srv.Information.Version);   // connection is established
            string outputfile = @"c:\temp\output.sql";

            Database db = new Database();
            db = srv.Databases[dbName];

            Scripter scr = new Scripter(srv);
            srv.SetDefaultInitFields(typeof(View), "IsSystemObject");

            ScriptingOptions options = new ScriptingOptions();
            options.DriAll = true;
            options.ClusteredIndexes = true;
            options.Default = true;
            options.DriAll = true;
            options.Indexes = true;
            options.IncludeHeaders = true;
            options.AppendToFile = false;
            options.FileName = outputfile;
            options.ToFileOnly = true;
            scr.Options = options;

            //Table[] tbls = new Table[db.Tables.Count];
            //db.Tables.CopyTo(tbls, 0);
            //scr.Script(tbls);

            //options.AppendToFile = true;
            //View[] view = new View[1];
            //for (int idx = 0; idx < db.Views.Count; idx++)
            //{
            //    if (!db.Views[idx].IsSystemObject)
            //    {
            //        view[0] = db.Views[idx];
            //        scr.Script(view);
            //    }
            //}

            //DependencyTree tree = scr.DiscoverDependencies(tbls, true);
            //DependencyWalker depwalker = new Microsoft.SqlServer.Management.Smo.DependencyWalker();
            //DependencyCollection depcoll = depwalker.WalkDependencies(tree);

            //StreamWriter sw = new StreamWriter(outputfile, true, Encoding.Unicode);

            //StringBuilder sb = new StringBuilder();
            //foreach (DependencyCollectionNode dep in depcoll)
            //{
            //    sb.AppendFormat("EXEC sp_generate_inserts @table_name='{0}', @owner='dbo'{1}", dep.Urn.GetAttribute("Name"), Environment.NewLine);
            //}

            //DataSet ds = new DataSet();
            //ds = db.ExecuteWithResults(sb.ToString());
            //foreach (DataTable dt in ds.Tables)
            //{
            //    foreach (DataRow dr in dt.Rows)
            //    {
            //        sw.WriteLine(dr[0].ToString());
            //    }
            //}
            //sw.Close();



            //Disconnect
           srvConn.Disconnect();
        }
    }
}
